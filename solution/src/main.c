#include "image.h"
#include "bmp.h"
#include "rotate.h"
#include "util.h"
#include <stdlib.h>

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

int main(int argc, char *argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <source> <transformed> <rotation>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char *src_image_filename = argv[1];
    const char *trg_image_filename = argv[2];
    int rotation_angle = atoi(argv[3]);

    if (rotation_angle % 90 != 0 && rotation_angle < -270) {
        fprintf(stderr, "Enter a valid angle\n");
        return EXIT_FAILURE;
    }

    FILE *src_file = open_file(src_image_filename, "rb");
    if (src_file == NULL) {
        fprintf(stderr, "Error opening source image file.\n");
        return EXIT_FAILURE;
    }

    struct image src_image;
    if (from_bmp(src_file, &src_image) != READ_OK) {
        fprintf(stderr, "Error reading source image.\n");
        close_file(src_file);
        return EXIT_FAILURE;
    }

    close_file(src_file);

    struct image trg_image = rotate(&src_image, rotation_angle);

    FILE *trg_file = open_file(trg_image_filename, "wb");
    if (trg_file == NULL) {
        fprintf(stderr, "Error opening transformed image file.\n");
        free_image(&src_image);
        free_image(&trg_image);
        return EXIT_FAILURE;
    }

    if (to_bmp(trg_file, &trg_image) != WRITE_OK) {
        fprintf(stderr, "Error writing transformed image.\n");
        close_file(trg_file);
        free_image(&src_image);
        free_image(&trg_image);
        return EXIT_FAILURE;
    }

    close_file(trg_file);
    free_image(&src_image);
    free_image(&trg_image);

    return EXIT_SUCCESS;
}
