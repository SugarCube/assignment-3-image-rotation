#include "bmp.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

#define BMP_HEADER_SIZE 40
#define BITS_PER_PIXEL 24
#define BMP_FILE_TYPE 0x4D42
#define BMP_COLOR_PLANES 1
#define BMP_COMPRESSION 0
#define PIXEL_SIZE sizeof(struct pixel)

// Helper function to calculate padding
static size_t c_padding(size_t width) {
    return width % 4 == 0 ? 0 : 4 - (width * 3) % 4;
}

static struct bmp_header create_bmp_header(struct image const *image) {
    struct bmp_header header = {0};

    size_t padding = c_padding(image->width);

    header.bfType = BMP_FILE_TYPE;
    header.bfileSize = sizeof(struct bmp_header) + (PIXEL_SIZE * image->width + padding) * image->height;
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BMP_HEADER_SIZE;
    header.biHeight = image->height;
    header.biWidth = image->width;
    header.biPlanes = BMP_COLOR_PLANES;
    header.biBitCount = BITS_PER_PIXEL;
    header.biCompression = BMP_COMPRESSION;
    header.biSizeImage = (image->width * PIXEL_SIZE + padding) * image->height;
    header.biXPelsPerMeter = 1;
    header.biYPelsPerMeter = 1;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}

enum read_status from_bmp(FILE *source_file, struct image *image) {
    struct bmp_header header;

    if (fread(&header, sizeof(struct bmp_header), 1, source_file) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != BITS_PER_PIXEL) {
        return READ_INVALID_BITS;
    }

    const size_t width = header.biWidth;
    const size_t height = header.biHeight;

    image->width = width;
    image->height = height;

    image->data = malloc(PIXEL_SIZE * width * height);
    if (!image->data) {
        return READ_ERROR;
    }

    const size_t padding = c_padding(width);
    for (size_t y = 0; y < height; y++) {
        if (fread(&image->data[y * width], PIXEL_SIZE, width, source_file) != width) {
            free(image->data);
            return READ_ERROR;
        }
        if (fseek(source_file, (long)padding, SEEK_CUR) != 0) {
            perror("Error seeking in file");
            free(image->data);
            return READ_ERROR;
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *target_file, struct image const *image) {
    struct bmp_header header = create_bmp_header(image);

    const size_t padding = c_padding(image->width);

    if (fwrite(&header, sizeof(struct bmp_header), 1, target_file) != 1) {
        return WRITE_ERROR;
    }

    for (size_t y = 0; y < image->height; y++) {
        if (fwrite(&image->data[image->width * y], PIXEL_SIZE, image->width, target_file) != image->width) {
            return WRITE_ERROR;
        }
        if (fseek(target_file, (long)padding, SEEK_CUR) != 0) {
            perror("Error seeking in file");
            free(image->data);
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
