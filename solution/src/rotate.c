#include "rotate.h"
#include <stdlib.h>
#include <string.h>

// Нормирование угла так, чтобы он находился в диапазоне [0, 360)
static int normalize_angle(int angle) {
    return (angle % 360 + 360) % 360;
}

// Поворот на 180 градусов
static void rotate_180(const struct pixel *src, struct pixel *rotated, int width, int height) {
    int tmp_counter = 0;
    for (int i = width * height - 1; i >= 0; i--) {
        rotated[tmp_counter++] = src[i];
    }
}

// Поворот на 90 градусов по часовой стрелке
static void rotate_90_clockwise(const struct pixel *src, struct pixel *rotated, int width, int height) {
    int tmp_counter = 0;
    for (int y = width - 1; y >= 0; y--) {
        for (int x = 0; x < height; x++) {
            rotated[tmp_counter++] = src[x * width + y];
        }
    }
}

// Поворот на 270 градусов по часовой стрелке
static void rotate_270_clockwise(const struct pixel *src, struct pixel *rotated, int width, int height) {
    int tmp_counter = 0;
    for (int y = 0; y < width; y++) {
        for (int x = height - 1; x >= 0; x--) {
            rotated[tmp_counter++] = src[x * width + y];
        }
    }
}

// Поворот изображения на заданный угол
static struct image rotate_pixels(const struct image *src, int angle) {
    struct image rotated = {0};

    angle = normalize_angle(angle);

    rotated.width = (angle == 90 || angle == 270) ? src->height : src->width;
    rotated.height = (angle == 90 || angle == 270) ? src->width : src->height;

    rotated.data = malloc(rotated.width * rotated.height * sizeof(struct pixel));
    if (!rotated.data) {
        // Handle memory allocation failure
        return (struct image){0};
    }

    if (angle == 0) {
        memcpy(rotated.data, src->data, src->width * src->height * sizeof(struct pixel));
    } else if (angle == 180) {
        // Поворот на 180 градусов
        rotate_180(src->data, rotated.data, (int)src->width, (int)src->height);
    } else if (angle == 90) {
        // Поворот на 90 градусов по часовой стрелке
        rotate_90_clockwise(src->data, rotated.data, (int)src->width, (int)src->height);
    } else if (angle == 270) {
        // Поворот на 270 градусов по часовой стрелке
        rotate_270_clockwise(src->data, rotated.data, (int)src->width, (int)src->height);
    }

    return rotated;
}

struct image rotate(const struct image *src, int angle) {
    if (src) {
        return rotate_pixels(src, angle);
    }
    return (struct image){0};
}
