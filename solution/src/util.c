#include "util.h"

FILE *open_file(const char *filename, const char *mode) {
    FILE *file = fopen(filename, mode);
    if (file == NULL) {
        perror("Error opening file");
    }
    return file;
}

void close_file(FILE *file) {
    if (fclose(file) != 0) {
        perror("Error closing file");
    }
}
