#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>

FILE *open_file(const char *filename, const char *mode);

void close_file(FILE *file);

#endif // UTIL_H
