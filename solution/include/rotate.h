#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"

struct image rotate(struct image const* src, int angle);


#endif // ROTATE_H
